import 'package:flutter/material.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:provider/provider.dart';
import 'package:vare_app_demo/theme/themeNotifier.dart';

class DrawerWidget extends StatefulWidget {
  @override
  _DrawerWidgetState createState() => _DrawerWidgetState();
}

extension HexColo on Color {
  /// String is in the format "aabbcc" or "ffaabbcc" with an optional leading "#".
  // static Color fromHex(String hexString) {
  //   final buffer = StringBuffer();
  //   if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
  //   buffer.write(hexString.replaceFirst('#', ''));
  //   return Color(int.parse(buffer.toString(), radix: 16));
  // }

  /// Prefixes a hash sign if [leadingHashSign] is set to `true` (default is `true`).
  String toHex({bool leadingHashSign = true}) => '${leadingHashSign ? '#' : ''}'
      '${alpha.toRadixString(16).padLeft(2, '0')}'
      '${red.toRadixString(16).padLeft(2, '0')}'
      '${green.toRadixString(16).padLeft(2, '0')}'
      '${blue.toRadixString(16).padLeft(2, '0')}';
}

class _DrawerWidgetState extends State<DrawerWidget> {
  @override
  Widget build(BuildContext context) {
    final themeNotifier = Provider.of<ThemeNotifier>(context);
    return Drawer(
      child: SafeArea(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            ListTile(
              // leading: ,

              title:
                  Text(' Change Colors', style: TextStyle(color: Colors.black)),
            ),
            Consumer<ThemeNotifier>(
              // stream: null,
              builder: (context,  themeN, body) {
                return ListTile(
                  // leading: ,
                  trailing: Icon(
                    Icons.color_lens,
                    color: Colors.black,
                  ),
                  title: Text(' Primary/AppBar Color',
                      style: TextStyle(color: Colors.black)),
                  onTap: () {
                    Color pickerColor = Theme.of(context).primaryColor;
                    Color currentColor = Theme.of(context).primaryColor;
                    double f = themeN.getF;
                    double s = themeN.getS;
                    Color pickerColorS = Theme.of(context).primaryColorLight;
                    Color currentColorS = Theme.of(context).primaryColorLight;
// ValueChanged<Color> callback
                    void changeColor(Color color) {
                      setState(() => pickerColor = color);
                      themeNotifier.setPrimary(color,);
                    }
                    void changeColorS(Color color) {
                      setState(() => pickerColorS = color);
                      themeNotifier.setColorS(color,);
                    }
// raise the [showDialog] widget
                    showDialog(
                      context: context,
                      builder: (context) => AlertDialog(
                        title: const Text('Pick a Gradient!'),
                        content: SingleChildScrollView(
                          child: Column(
                            children: [
                              ColorPicker(
                                labelTextStyle: TextStyle(color: Colors.black),
                                enableAlpha: false,
                                pickerColor: pickerColor,
                                onColorChanged: changeColor,
                                showLabel: true,
                                pickerAreaHeightPercent: 0.5,
                              ),
                              Text(
                                  Theme.of(context)
                                      .primaryColor
                                      .toHex()
                                      .replaceAll("#ff", "#"),
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline4
                                      .copyWith(fontSize: 14)),
                              TextField(
                                maxLength: 6,
                                onChanged: (s) {
                                  if (s.length == 6) {
                                    changeColor(HexColor(s));
                                  }
                                },
                                decoration: InputDecoration(
                                    labelText: " Or Enter Hex Color for first Color",
                                    labelStyle: Theme.of(context)
                                        .textTheme
                                        .headline4
                                        .copyWith(fontSize: 14)),
                              ),
                              // Text(
                              //     "Enter end point for first color ",
                              //     style: Theme.of(context)
                              //         .textTheme
                              //         .headline4
                              //         .copyWith(fontSize: 14)),
                              TextField(
                                // maxLength: 6,
                                keyboardType: TextInputType.number,
                                onChanged: (s) {
                                  if (s.isNotEmpty) {
                                    themeNotifier.setF(double.parse(s.toString()));
                                  }
                                },
                                decoration: InputDecoration(
                                    labelText: "Enter end point for first color ${themeN.getF}",
                                    hintText: "eg: "+themeN.getF.toString(),
                                    labelStyle: Theme.of(context)
                                        .textTheme
                                        .headline4
                                        .copyWith(fontSize: 14)),
                              ),
                              SizedBox(height: 10,),
                              ColorPicker(
                                labelTextStyle: TextStyle(color: Colors.black),
                                enableAlpha: false,
                                pickerColor: pickerColorS,
                                onColorChanged: changeColorS,
                                showLabel: true,
                                pickerAreaHeightPercent: 0.5,
                              ),
                              Text(
                                  Theme.of(context)
                                      .primaryColor
                                      .toHex()
                                      .replaceAll("#ff", "#"),
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline4
                                      .copyWith(fontSize: 14)),
                              TextField(
                                maxLength: 6,
                                onChanged: (s) {
                                  if (s.length == 6) {
                                    changeColorS(HexColor(s));
                                  }
                                },
                                decoration: InputDecoration(
                                    labelText: " Or Enter Hex Color for second Color",
                                    labelStyle: Theme.of(context)
                                        .textTheme
                                        .headline4
                                        .copyWith(fontSize: 14)),
                              ),
                              // Text(
                              //     "Enter end point for second color ",
                              //     style: Theme.of(context)
                              //         .textTheme
                              //         .headline4
                              //         .copyWith(fontSize: 14)),
                              TextField(
                                // maxLength: 6,
                                keyboardType: TextInputType.number,
                                onChanged: (s) {
                                  if (s.isNotEmpty) {
                                    themeNotifier.setF(double.parse(s.toString()));
                                  }
                                },
                                decoration: InputDecoration(
                                    labelText: "Enter end point for second color ${themeN.getS}",
                                    hintText: "eg: "+themeN.getS.toString(),
                                    labelStyle: Theme.of(context)
                                        .textTheme
                                        .headline4
                                        .copyWith(fontSize: 14)),
                              ),
SizedBox(height: 10,)
                            ],
                          ),
                        ),
                        actions: <Widget>[
                          TextButton(
                            child: const Text('Done'),
                            onPressed: () {
                              setState(() => currentColor = pickerColor);
                              Navigator.of(context).pop();
                            },
                          ),
                        ],
                      ),
                    );
                  },
                );
              }
            ),
            ListTile(
              // leading: ,
              trailing: Icon(
                Icons.color_lens,
                color: Colors.black,
              ),
              title: Text(' Secondary Color',
                  style: TextStyle(color: Colors.black)),
              onTap: () {
                Color pickerColor = Theme.of(context).accentColor;
                Color currentColor = Theme.of(context).accentColor;

// ValueChanged<Color> callback
                void changeColor(Color color) {
                  setState(() => pickerColor = color);
                  themeNotifier.setSecondary(color);
                }

// raise the [showDialog] widget
                showDialog(
                  context: context,
                  builder: (context) => AlertDialog(
                    title: const Text('Pick Secondary Color!'),
                    content: SingleChildScrollView(
                      child: Column(
                        children: [
                          ColorPicker(
                            labelTextStyle: TextStyle(color: Colors.black),
                            enableAlpha: false,
                            pickerColor: pickerColor,
                            onColorChanged: changeColor,
                            showLabel: true,
                            pickerAreaHeightPercent: 0.8,
                          ),
                          Text(
                              Theme.of(context)
                                  .accentColor
                                  .toHex()
                                  .replaceAll("#ff", "#"),
                              style: Theme.of(context)
                                  .textTheme
                                  .headline4
                                  .copyWith(fontSize: 14)),
                          TextField(
                            maxLength: 6,
                            onChanged: (s) {
                              if (s.length == 6) {
                                changeColor(HexColor(s));
                              }
                            },
                            decoration: InputDecoration(
                                labelText: "Or Enter Hex Color",
                                labelStyle: Theme.of(context)
                                    .textTheme
                                    .headline4
                                    .copyWith(fontSize: 14)),
                          )
                        ],
                      ),
                    ),
                    actions: <Widget>[
                      TextButton(
                        child: const Text('Done'),
                        onPressed: () {
                          setState(() => currentColor = pickerColor);
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  ),
                );
              },
            ),
            ListTile(
              // leading: ,
              trailing: Icon(
                Icons.color_lens,
                color: Colors.black,
              ),
              title:
                  Text(' Background ', style: TextStyle(color: Colors.black)),
              onTap: () {
                Color pickerColor = Theme.of(context).backgroundColor;
                Color currentColor = Theme.of(context).backgroundColor;

// ValueChanged<Color> callback
                void changeColor(Color color) {
                  setState(() => pickerColor = color);
                  themeNotifier.setBackgroud(color);
                }

// raise the [showDialog] widget
                showDialog(
                  context: context,
                  builder: (context) => AlertDialog(
                    title: const Text('Pick Background Color!'),
                    content: SingleChildScrollView(
                      child: Column(
                        children: [
                          ColorPicker(
                            labelTextStyle: TextStyle(color: Colors.black),
                            enableAlpha: false,
                            pickerColor: pickerColor,
                            onColorChanged: changeColor,
                            showLabel: true,
                            pickerAreaHeightPercent: 0.8,
                          ),
                          Text(
                              Theme.of(context)
                                  .backgroundColor
                                  .toHex()
                                  .replaceAll("#ff", "#"),
                              style: Theme.of(context)
                                  .textTheme
                                  .headline4
                                  .copyWith(fontSize: 16)),
                          TextField(
                            maxLength: 6,
                            onChanged: (s) {
                              if (s.length == 6) {
                                changeColor(HexColor(s));
                              }
                            },
                            decoration: InputDecoration(
                                // hintText:  Theme.of(context).backgroundColor.toHex().replaceAll("#ff", "#"),
                                labelText: "Or Enter Hex Color",
                                labelStyle: Theme.of(context)
                                    .textTheme
                                    .headline4
                                    .copyWith(fontSize: 14)),
                          )
                        ],
                      ),
                    ),
                    actions: <Widget>[
                      TextButton(
                        child: const Text('Done'),
                        onPressed: () {
                          setState(() => currentColor = pickerColor);
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  ),
                );
              },
            ),
            ListTile(
              // leading: ,
              trailing: Icon(
                Icons.color_lens,
                color: Colors.black,
              ),
              title: Text(' Add Cart Button ',
                  style: TextStyle(color: Colors.black)),
              onTap: () {
                Color pickerColor = Theme.of(context).buttonColor;
                Color currentColor = Theme.of(context).buttonColor;

// ValueChanged<Color> callback
                void changeColor(Color color) {
                  setState(() => pickerColor = color);
                  themeNotifier.setCartButton(color);
                }

// raise the [showDialog] widget
                showDialog(
                  context: context,
                  builder: (context) => AlertDialog(
                    title: const Text('Pick Button Color!'),
                    content: SingleChildScrollView(
                      child: Column(
                        children: [
                          ColorPicker(
                            labelTextStyle: TextStyle(color: Colors.black),
                            enableAlpha: false,
                            pickerColor: pickerColor,
                            onColorChanged: changeColor,
                            showLabel: true,
                            pickerAreaHeightPercent: 0.8,
                          ),
                          Text(
                              Theme.of(context)
                                  .buttonColor
                                  .toHex()
                                  .replaceAll("#ff", "#"),
                              style: Theme.of(context)
                                  .textTheme
                                  .headline4
                                  .copyWith(fontSize: 14)),
                          TextField(
                            maxLength: 6,
                            onChanged: (s) {
                              if (s.length == 6) {
                                changeColor(HexColor(s));
                              }
                            },
                            decoration: InputDecoration(
                                labelText: "Or Enter Hex Color",
                                labelStyle: Theme.of(context)
                                    .textTheme
                                    .headline4
                                    .copyWith(fontSize: 14)),
                          )
                        ],
                      ),
                    ),
                    actions: <Widget>[
                      TextButton(
                        child: const Text('Done'),
                        onPressed: () {
                          setState(() => currentColor = pickerColor);
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  ),
                );
              },
            ),
            ListTile(
              // leading: ,
              trailing: Icon(
                Icons.color_lens,
                color: Colors.black,
              ),
              title:
                  Text(' Title text ', style: TextStyle(color: Colors.black)),
              onTap: () {
                Color pickerColor = Theme.of(context).textTheme.headline6.color;
                Color currentColor =
                    Theme.of(context).textTheme.headline6.color;

// ValueChanged<Color> callback
                void changeColor(Color color) {
                  setState(() => pickerColor = color);
                  themeNotifier.setTitleTextColor(color);
                }

// raise the [showDialog] widget
                showDialog(
                  context: context,
                  builder: (context) => AlertDialog(
                    title: const Text('Pick Title Color!'),
                    content: SingleChildScrollView(
                      child: Column(
                        children: [
                          ColorPicker(
                            labelTextStyle: TextStyle(color: Colors.black),
                            enableAlpha: false,
                            pickerColor: pickerColor,
                            onColorChanged: changeColor,
                            showLabel: true,
                            pickerAreaHeightPercent: 0.8,
                          ),
                          Text(
                              Theme.of(context)
                                  .textTheme
                                  .headline6
                                  .color
                                  .toHex()
                                  .replaceAll("#ff", "#"),
                              style: Theme.of(context)
                                  .textTheme
                                  .headline4
                                  .copyWith(fontSize: 14)),
                          TextField(
                            maxLength: 6,
                            onChanged: (s) {
                              if (s.length == 6) {
                                changeColor(HexColor(s));
                              }
                            },
                            decoration: InputDecoration(
                                labelText: "Or Enter Hex Color",
                                labelStyle: Theme.of(context)
                                    .textTheme
                                    .headline4
                                    .copyWith(fontSize: 14)),
                          )
                        ],
                      ),
                    ),
                    actions: <Widget>[
                      TextButton(
                        child: const Text('Done'),
                        onPressed: () {
                          setState(() => currentColor = pickerColor);
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  ),
                );
              },
            ),
            ListTile(
              // leading: ,
              trailing: Icon(
                Icons.color_lens,
                color: Colors.black,
              ),
              title: Text(' SubTitle text ',
                  style: TextStyle(color: Colors.black)),
              onTap: () {
                Color pickerColor = Theme.of(context).textTheme.subtitle1.color;
                Color currentColor =
                    Theme.of(context).textTheme.subtitle1.color;

// ValueChanged<Color> callback
                void changeColor(Color color) {
                  setState(() => pickerColor = color);
                  themeNotifier.setSubTitleTextColor(color);
                }

// raise the [showDialog] widget
                showDialog(
                  context: context,
                  builder: (context) => AlertDialog(
                    title: const Text('Pick SubTitle Color!'),
                    content: SingleChildScrollView(
                      child: Column(
                        children: [
                          ColorPicker(
                            labelTextStyle: TextStyle(color: Colors.black),
                            enableAlpha: false,
                            pickerColor: pickerColor,
                            onColorChanged: changeColor,
                            showLabel: true,
                            pickerAreaHeightPercent: 0.8,
                          ),
                          Text(
                              Theme.of(context)
                                  .textTheme
                                  .subtitle1
                                  .color
                                  .toHex()
                                  .replaceAll("#ff", "#"),
                              style: Theme.of(context)
                                  .textTheme
                                  .headline4
                                  .copyWith(fontSize: 14)),
                          TextField(
                            maxLength: 6,
                            onChanged: (s) {
                              if (s.length == 6) {
                                changeColor(HexColor(s));
                              }
                            },
                            decoration: InputDecoration(
                              labelText: "Or Enter Hex Color",
                              labelStyle: Theme.of(context)
                                  .textTheme
                                  .headline4
                                  .copyWith(fontSize: 14),
                            ),
                          )
                        ],
                      ),
                    ),
                    actions: <Widget>[
                      TextButton(
                        child: const Text('Done'),
                        onPressed: () {
                          setState(() => currentColor = pickerColor);
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  ),
                );
              },
            ),
            ListTile(
              // leading: ,
              trailing: Icon(
                Icons.color_lens,
                color: Colors.black,
              ),
              title: Text(' Over Category text ',
                  style: TextStyle(color: Colors.black)),
              onTap: () {
                Color pickerColor = Theme.of(context).textTheme.headline1.color;
                Color currentColor =
                    Theme.of(context).textTheme.headline1.color;

// ValueChanged<Color> callback
                void changeColor(Color color) {
                  setState(() => pickerColor = color);
                  themeNotifier.setoverCategory(color);
                }

// raise the [showDialog] widget
                showDialog(
                  context: context,
                  builder: (context) => AlertDialog(
                    title: const Text('Pick Over Category Color!'),
                    content: SingleChildScrollView(
                      child: Column(
                        children: [
                          ColorPicker(
                            labelTextStyle: TextStyle(color: Colors.black),
                            enableAlpha: false,
                            pickerColor: pickerColor,
                            onColorChanged: changeColor,
                            showLabel: true,
                            pickerAreaHeightPercent: 0.8,
                          ),
                          Text(
                              Theme.of(context)
                                  .textTheme
                                  .headline1
                                  .color
                                  .toHex()
                                  .replaceAll("#ff", "#"),
                              style: Theme.of(context)
                                  .textTheme
                                  .headline4
                                  .copyWith(fontSize: 14)),
                          TextField(
                            maxLength: 6,
                            onChanged: (s) {
                              if (s.length == 6) {
                                changeColor(HexColor(s));
                              }
                            },
                            decoration: InputDecoration(
                              labelText: "Or Enter Hex Color",
                              labelStyle: Theme.of(context)
                                  .textTheme
                                  .headline4
                                  .copyWith(fontSize: 14),
                            ),
                          )
                        ],
                      ),
                    ),
                    actions: <Widget>[
                      TextButton(
                        child: const Text('Done'),
                        onPressed: () {
                          setState(() => currentColor = pickerColor);
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  ),
                );
              },
            ),
            ListTile(
              // leading: ,
              trailing: Icon(
                Icons.color_lens,
                color: Colors.black,
              ),
              title: Text(' AppBar text ',
                  style: TextStyle(color: Colors.black)),
              onTap: () {
                Color pickerColor = Theme.of(context).primaryColorDark;
                Color currentColor =
                    Theme.of(context).primaryColorDark;

// ValueChanged<Color> callback
                void changeColor(Color color) {
                  setState(() => pickerColor = color);
                  themeNotifier.setAppText(color);
                }

// raise the [showDialog] widget
                showDialog(
                  context: context,
                  builder: (context) => AlertDialog(
                    title: const Text('Pick Over Category Color!'),
                    content: SingleChildScrollView(
                      child: Column(
                        children: [
                          ColorPicker(
                            labelTextStyle: TextStyle(color: Colors.black),
                            enableAlpha: false,
                            pickerColor: pickerColor,
                            onColorChanged: changeColor,
                            showLabel: true,
                            pickerAreaHeightPercent: 0.8,
                          ),
                          Text(
                              Theme.of(context)
                                  .textTheme
                                  .headline1
                                  .color
                                  .toHex()
                                  .replaceAll("#ff", "#"),
                              style: Theme.of(context)
                                  .textTheme
                                  .headline4
                                  .copyWith(fontSize: 14)),
                          TextField(
                            maxLength: 6,
                            onChanged: (s) {
                              if (s.length == 6) {
                                changeColor(HexColor(s));
                              }
                            },
                            decoration: InputDecoration(
                              labelText: "Or Enter Hex Color",
                              labelStyle: Theme.of(context)
                                  .textTheme
                                  .headline4
                                  .copyWith(fontSize: 14),
                            ),
                          )
                        ],
                      ),
                    ),
                    actions: <Widget>[
                      TextButton(
                        child: const Text('Done'),
                        onPressed: () {
                          setState(() => currentColor = pickerColor);
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
