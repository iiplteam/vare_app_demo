import 'dart:ui';

import 'package:flutter/material.dart';



/// ----  Spooky Theme  ----
final spookyPrimary = Color(0xFF70C2DF);
final spookyAccent = Color(0xFFBB86FC);
final spookyBackground = Color(0xFF4A4A4A);

final spookyColor = Color(0xFFFFFFFF);
final spookySecond=Colors.black;
final spookyTheme = ThemeData(
  primaryColor: spookyPrimary,
  accentColor: spookyAccent,
  primaryColorLight:spookySecond ,
  primaryColorDark: Colors.white,
  // text
  buttonColor: Colors.orange,
  textTheme: TextTheme(
    subtitle1: TextStyle(color:  Colors.black),
    subtitle2: TextStyle(color:  Colors.black),
    headline1: TextStyle(color:  Colors.black),
    headline6: TextStyle(color: Colors.black),
    bodyText1: TextStyle(color:  Colors.black),
    bodyText2: TextStyle(color:  Colors.black),
  ),
  backgroundColor: Color(0xFFfff6f4),
);
