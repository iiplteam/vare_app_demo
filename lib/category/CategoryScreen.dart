import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:vare_app_demo/category/categoryWidget.dart';
import 'package:vare_app_demo/helpers/sizeConfig.dart';
import 'package:vare_app_demo/theme/themeNotifier.dart';

class CategoryScreen extends StatefulWidget {
  @override
  _CategoryScreenState createState() => _CategoryScreenState();
}

class _CategoryScreenState extends State<CategoryScreen> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return  Consumer<ThemeNotifier>(
      // stream: null,
        builder: (context,  themeNotifier, body) {
          return  Scaffold(
            // bottomNavigationBar: BottomNavBar(
            //   index: 0,
            //   isLogin: isLogin,
            // ),
            backgroundColor:Theme.of(context).backgroundColor,
            appBar:
                // AppBarWithPop(context, "Products",
                //     needSearch: true, needCart: true, ispop: false)
                AppBar(
                  iconTheme: IconThemeData(color: Theme.of(context).primaryColorDark),
                  flexibleSpace: Container(
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        colors: [Theme.of(context).primaryColor,  Theme.of(context).primaryColorLight],
                        begin: Alignment.centerLeft,
                        end: Alignment.centerRight,
                        stops: [
                          themeNotifier.getF,
                          themeNotifier.getS
                          //  -0.2,
                          // 2.1,
                          // 0.6,
                          // 0.9,
                        ],
                      ),
                    ),
                  ),
              title: Text("Categories",
                  style: TextStyle(color: Theme.of(context).primaryColorDark),
              ),
            ),
            body: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  // Divider(thickness: 1.2, height: 1.2, color: Colors.grey[300]),

                  Expanded(
                      child: ListView.builder(
                          physics: BouncingScrollPhysics(),
                          // controller: controller,
                          padding: EdgeInsets.symmetric(vertical: 1),
                          scrollDirection: Axis.vertical,
                          shrinkWrap: true,
                          primary: false,
                          itemCount: 4,
                          // separatorBuilder: (context, index) {
                          //   return Container( height: 15,);
                          // },
                          itemBuilder: (context, index) {
                            return CategoryWidget(index:index);
                          }))
                ]));
      }
    );
  }
}
