import 'package:flutter/material.dart';
import 'package:vare_app_demo/helpers/sizeConfig.dart';

class CategoryBox extends StatelessWidget {
  int index;
  bool isView = false;
  Function viewAllPressed;

  CategoryBox({Key key, this.index, this.isView}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    return Container(
        decoration: BoxDecoration(
          color: Theme.of(context).primaryColorLight,
          borderRadius: BorderRadius.all(Radius.circular(15)),
        ),
        height: SizeConfig.w * 0.2,
        width: SizeConfig.w * 0.3,
        child: Stack(
          children: [
            Container(
                height: SizeConfig.w * 0.2,
                //
                width: SizeConfig.w * 0.3,
                decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black.withOpacity(0.3),
                        blurRadius: 2,
                        offset: Offset(2.5, 1.5)),
                  ],
                  // color: Theme.of(context).primaryColorLight,
                  borderRadius: BorderRadius.all(Radius.circular(15)),
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(15)),
                  child: Image.asset(
                    "assets/cat${index + 1}.jpg",
                    fit: BoxFit.cover,
                  ),
                )),
            Positioned(
                top: 5,
                left: 10,
                child: Text("Category",
                    style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.w600,
                        color: Theme.of(context).textTheme.headline1.color))),
            Positioned(
              top: 20,
              left: 10,
              child: Text("Subtitle",
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.w500,
                    color: Theme.of(context).textTheme.headline1.color,
                  )),
            ),
          ],
        ));
  }
}
