import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:vare_app_demo/helpers/sizeConfig.dart';

class CategoryWidget extends StatefulWidget {
  int index;
  CategoryWidget({this.index});
  @override
  _CategoryWidgetState createState() => _CategoryWidgetState();
}

class _CategoryWidgetState extends State<CategoryWidget> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
      height: SizeConfig.h * 0.26,

      // height: SizeConfig.h*0.2,
      child: Stack(
        children: [
          Container(
            height: SizeConfig.h * 0.26,
            width: double.infinity,
            child: Image.asset(
              "assets/cat${widget.index + 1}.jpg",
              fit: BoxFit.fitWidth,
            ),
          ),
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ListTile(
                  title: Text(
                    "category",

                    style:
                        // TextStyle(color:
                        Theme.of(context).textTheme.subtitle1.copyWith(
                            color: Theme.of(context).textTheme.headline1.color),
                    // ),
                  ),
                  subtitle: Text(
                    "sub Title",
                    style:
                        // TextStyle(color:
                        Theme.of(context).textTheme.subtitle2.copyWith(
                            color: Theme.of(context).textTheme.headline1.color),
                  ),
                ),
                // RatingBar.builder(
                //   initialRating:   Random().nextInt(5) .toDouble(),
                //   minRating: 1,
                //   direction: Axis.horizontal,
                //   allowHalfRating: true,
                //   itemCount: 5,
                //   itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                //   itemBuilder: (context, _) => Icon(
                //     Icons.star,
                //     color: Colors.amber,
                //   ),
                //   onRatingUpdate: (rating) {
                //     print(rating);
                //   },
                // )
              ],
            ),
          )
        ],
      ),
    );
  }
}
