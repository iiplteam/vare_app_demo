import 'package:flutter/material.dart';
import 'package:vare_app_demo/theme/theme.dart';

class ThemeNotifier with ChangeNotifier {
  ThemeData _themeData = spookyTheme;

  ThemeNotifier(this._themeData);
  double fStart =-0.2, sStart =2.1;

  getTheme() => _themeData;

  double get getF => fStart;

  double get getS => sStart;
setAppText(Color color)async {
  _themeData = _themeData.copyWith(primaryColorDark: color);
  notifyListeners();
}

setPrimary(Color colorF) async {
    _themeData = _themeData.copyWith(
      primaryColor: colorF,
    );

    // sStart=s;

    notifyListeners();
  }

  setColorS(Color color) async {
    _themeData = _themeData.copyWith(primaryColorLight: color);
    notifyListeners();
  }

  setF(double F) async {
    fStart = F;
    notifyListeners();
  }

  setS(double S) async {
    fStart = S;
    notifyListeners();
  }

  setBackgroud(Color color) async {
    _themeData = _themeData.copyWith(backgroundColor: color);
    notifyListeners();
  }

  setCartButton(Color color) async {
    _themeData = _themeData.copyWith(buttonColor: color);
    notifyListeners();
  }

  setSecondary(Color color) async {
    _themeData = _themeData.copyWith(accentColor: color);
    notifyListeners();
  }

  setTitleTextColor(Color color) async {
    _themeData = _themeData.copyWith(
        textTheme: _themeData.textTheme.copyWith(
      headline6: TextStyle(color: color),
    ));
    notifyListeners();
  }

  setSubTitleTextColor(Color color) async {
    _themeData = _themeData.copyWith(
        textTheme: _themeData.textTheme.copyWith(
      subtitle1: TextStyle(color: color),
      subtitle2: TextStyle(color: color),
      // bodyText1: TextStyle(color: color),
    ));
    notifyListeners();
  }

  setoverCategory(Color color) async {
    _themeData = _themeData.copyWith(
        textTheme: _themeData.textTheme.copyWith(
      headline1: TextStyle(color: color),
    ));
    notifyListeners();
  }

  setBodyTextColor(Color color) async {
    _themeData = _themeData.copyWith(
        textTheme: TextTheme(
      headline3: TextStyle(color: color),
    ));
    notifyListeners();
  }

  setTheme(ThemeData themeData) async {
    _themeData = themeData;
    notifyListeners();
  }
}
