import 'package:flutter/material.dart';

class HomeTitle extends StatelessWidget {
  String title;
  bool isView=false;
  Function viewAllPressed;
  HomeTitle({Key key, this.viewAllPressed, this.title,this.isView}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 15,
          // left: 20, right: 20,
          bottom: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(title,
                  style: TextStyle(
                      fontWeight: FontWeight.w500
                          ,fontSize: 20
                  ,    color: Theme.of(context).textTheme.headline6.color,

      ),
                 ),
              ( isView??false)?
              InkWell(
                onTap: viewAllPressed,
                child: Text(
                  "View All",
                  style: TextStyle(
                      fontWeight: FontWeight.w500
                      ,fontSize: 17,
                    color: Theme.of(context).textTheme.headline6.color,
                  ),

                ),
              ):Container(),
            ],
          ),
        ],
      ),
    );
  }
}
