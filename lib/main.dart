import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:provider/provider.dart';
import 'package:vare_app_demo/category/CategoryScreen.dart';
import 'package:vare_app_demo/drawer/DrawerWdget.dart';
import 'package:vare_app_demo/helpers/sizeConfig.dart';
import 'package:vare_app_demo/homeWidgets/categoryBox.dart';
import 'package:vare_app_demo/productDetail/threelayer.dart';
import 'package:vare_app_demo/theme/theme.dart';
import 'package:vare_app_demo/theme/themeNotifier.dart';

import 'homeWidgets/homeTitle.dart';

void main() {
  runApp(ChangeNotifierProvider<ThemeNotifier>(
      create: (_) => ThemeNotifier(spookyTheme), child: MyApp()));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final themeNotifier = Provider.of<ThemeNotifier>(context);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "Flutter Dynamic Theme",
      theme: themeNotifier.getTheme(),
      home: MyHomePage(title: "Vare App"),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    final themeNotifier = Provider.of<ThemeNotifier>(context);

    SizeConfig().init(context);
    return Scaffold(
      drawer: DrawerWidget(),

      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Theme.of(context).primaryColorDark),
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [Theme.of(context).primaryColor,  Theme.of(context).primaryColorLight],
              begin: Alignment.centerLeft,
              end: Alignment.centerRight,
              stops: [
                themeNotifier.getF,
                themeNotifier.getS
               //  -0.2,
               // 2.1,
                // 0.6,
                // 0.9,
              ],
            ),
          ),
        ),
        titleSpacing: 0,
        backgroundColor: Theme.of(context).appBarTheme.color,
        // leading:
        actions: [
          Icon(
            Icons.search,
            size: 35,
          )
        ],
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 3.0),
              child: Image.asset(
                "assets/vareIcon.png",
                height: 28,
              ),
            ),
            Text(widget.title,
            style: TextStyle(color: Theme.of(context).primaryColorDark),
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(left: 15, right: 15.0, top: 15),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                // margin: const EdgeInsets.only(
                //   bottom: 15,
                // ),
                constraints: BoxConstraints(maxWidth: double.infinity),
                decoration: BoxDecoration(
                  // color: Theme.of(context).primaryColorLight,
                  borderRadius: BorderRadius.all(Radius.circular(15)),
                ),
                height: SizeConfig.w * 0.3,
                width: double.infinity,
                child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(15)),
                  child: Image.asset(
                    "assets/banner.jpg",
                    fit: BoxFit.fitWidth,
                  ),
                ),
              ),
              HomeTitle(
                viewAllPressed: () => Navigator.push(context,
                    MaterialPageRoute(builder: (context) => CategoryScreen())),
                isView: true,
                title: "Categories",
              ),
              Container(
                height: SizeConfig.w * 0.2,
                child: ListView.separated(
                    addAutomaticKeepAlives: true,
                    padding: EdgeInsets.only(top: 10),
                    separatorBuilder: (
                      context,
                      i,
                    ) =>
                        Container(
                          width: 9,
                        ),
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    itemCount: 4,
                    itemBuilder: (context, i) => SizedBox(
                        height: SizeConfig.w * 0.2,
                        width: SizeConfig.w * 0.3,
                        child: CategoryBox(index: i))),
              ),
              HomeTitle(
                // isView: true,
                title: "Popular Products",
              ),
              StaggeredGridView.countBuilder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                crossAxisCount: 2,
                crossAxisSpacing: 10,
                mainAxisSpacing: 12,
                itemCount: 4,
                staggeredTileBuilder: (index) {
                  return StaggeredTile.count(1, !index.isEven ? 1.4 : 1.8);
                },
                itemBuilder: (context, index) {
                  final ran = Random().nextInt(50);
                  return Container(
                    // color: Colors.green,
                    child: Column(children: [
                      Container(
                        height: !index.isEven ? 200 : 280,
                        decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black.withOpacity(0.3),
                                blurRadius: 2,
                                offset: Offset(2.5, 1.5)),
                          ],
                          // color: Theme.of(context).primaryColorLight,
                          borderRadius: BorderRadius.all(Radius.circular(15)),
                        ),
                        child: ClipRRect(
                          // shape:  RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                          // ),
                          child: InkWell(
                            onTap: () => Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        EcommerceDetailThreePage(index: index
// price:

                                            ))),
                            child: Image.asset(
                              "assets/product${index + 1}.jpg",
                              fit: BoxFit.fill,
                              // height: 150,
                              width: SizeConfig.w * 0.42,
                            ),
                          ),
                        ),
                      ),
                      // ListTile(title:
                      Container(
                          padding: EdgeInsets.only(top: 10, left: 10),
                          alignment: Alignment.centerLeft,
                          child: Text(productList[index],
                              style: TextStyle(
                                  color: Theme.of(context)
                                      .textTheme
                                      .subtitle1
                                      .color))),
                      Container(
                          padding: EdgeInsets.only(top: 10, left: 10),
                          alignment: Alignment.centerLeft,
                          child: Text("\$ ${ran * (index + 1)}",
                              style: TextStyle(
                                  color: Theme.of(context)
                                      .textTheme
                                      .subtitle1
                                      .color))),

                      // ,)
                    ]),
                  );
                },
              ),
              HomeTitle(
                // isView: true,
                title: "Sponcers",
              ),
              Container(
                height: SizeConfig.w * 0.2,
                child: ListView.separated(
                    addAutomaticKeepAlives: true,
                    padding: EdgeInsets.only(top: 10),
                    separatorBuilder: (
                      context,
                      i,
                    ) =>
                        Container(
                          width: 9,
                        ),
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    itemCount: 4,
                    itemBuilder: (context, i) => SizedBox(
                        height: SizeConfig.w * 0.2,
                        width: SizeConfig.w * 0.3,
                        child: CategoryBox(index: i))),
              ),
              HomeTitle(
                // isView: true,
                title: "Best Products",
              ),
              StaggeredGridView.countBuilder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                crossAxisCount: 2,
                crossAxisSpacing: 10,
                mainAxisSpacing: 12,
                itemCount: 4,
                staggeredTileBuilder: (index) {
                  return StaggeredTile.count(1, index.isEven ? 1.4 : 1.8);
                },
                itemBuilder: (context, index) {
                  final ran = Random().nextInt(50);
                  return Container(
                    // color: Colors.green,
                    child: Column(children: [
                      Container(
                        height: index.isEven ? 200 : 280,
                        decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                                color: Colors.black.withOpacity(0.3),
                                blurRadius: 2,
                                offset: Offset(2.5, 1.5)),
                          ],
                          // color: Theme.of(context).primaryColorLight,
                          borderRadius: BorderRadius.all(Radius.circular(15)),
                        ),
                        child: ClipRRect(
                          // shape:  RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                          // ),
                          child: InkWell(
                            onTap: () => Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        EcommerceDetailThreePage(index: index
// price:

                                            ))),
                            child: Image.asset(
                              "assets/product${index + 1}.jpg",
                              fit: BoxFit.fill,
                              // height: 150,
                              width: SizeConfig.w * 0.42,
                            ),
                          ),
                        ),
                      ),
                      // ListTile(title:
                      Container(
                          padding: EdgeInsets.only(top: 10, left: 10),
                          alignment: Alignment.centerLeft,
                          child: Text(productList[index],
                              style: Theme.of(context).textTheme.subtitle1)),
                      Container(
                          padding: EdgeInsets.only(top: 10, left: 10),
                          alignment: Alignment.centerLeft,
                          child: Text("\$ ${ran * (index + 1)}",
                              style: Theme.of(context).textTheme.subtitle1)),

                      // ,)
                    ]),
                  );
                },
              ),
            ],
          ),
        ),
      ),
      // floatingActionButton: SpeedDial(
      //   /// both default to 16
      //   marginEnd: 18,
      //   marginBottom: 20,
      //   // animatedIcon: AnimatedIcons.menu_close,
      //   // animatedIconTheme: IconThemeData(size: 22.0),
      //   /// This is ignored if animatedIcon is non null
      //   icon: Icons.colorize_outlined,
      //   activeIcon: Icons.remove,
      //   // iconTheme: IconThemeData(color: Colors.grey[50], size: 30),
      //
      //   /// The label of the main button.
      //   // label: Text("Open Speed Dial"),
      //   /// The active label of the main button, Defaults to label if not specified.
      //   // activeLabel: Text("Close Speed Dial"),
      //   /// Transition Builder between label and activeLabel, defaults to FadeTransition.
      //   // labelTransitionBuilder: (widget, animation) => ScaleTransition(scale: animation,child: widget),
      //   /// The below button size defaults to 56 itself, its the FAB size + It also affects relative padding and other elements
      //   buttonSize: 56.0,
      //   visible: true,
      //
      //   /// If true user is forced to close dial manually
      //   /// by tapping main button and overlay is not rendered.
      //   closeManually: false,
      //   curve: Curves.bounceIn,
      //   overlayColor: Colors.black,
      //   overlayOpacity: 0.5,
      //   onOpen: () => print('OPENING DIAL'),
      //   onClose: () => print('DIAL CLOSED'),
      //   tooltip: 'Speed Dial',
      //   heroTag: 'speed-dial-hero-tag',
      //   backgroundColor: Colors.white,
      //   foregroundColor: Colors.black,
      //   elevation: 8.0,
      //   shape: CircleBorder(),
      //
      //   // orientation: SpeedDialOrientation.Up,
      //   // childMarginBottom: 2,
      //   // childMarginTop: 2,
      //   gradientBoxShape: BoxShape.circle,
      //
      //   children: [
      //     SpeedDialChild(
      //       child: Icon(Icons.color_lens_rounded),
      //       backgroundColor: Colors.red,
      //       // label: 'First',
      //       // labelStyle: TextStyle(fontSize: 18.0),
      //       onTap: () => themeNotifier.setTheme(spookyTheme),
      //       onLongPress: () => print('FIRST CHILD LONG PRESS'),
      //     ),
      //     SpeedDialChild(
      //       child: Icon(Icons.color_lens_sharp),
      //       backgroundColor: Colors.blue,
      //       // label: 'Second',
      //       // labelStyle: TextStyle(fontSize: 18.0),
      //       onTap: () => themeNotifier.setTheme(blueTheme),
      //       onLongPress: () => print('SECOND CHILD LONG PRESS'),
      //     ),
      //     SpeedDialChild(
      //       child: Icon(Icons.color_lens_outlined),
      //       backgroundColor: Colors.green,
      //       // label: 'Third',
      //       // labelStyle: TextStyle(fontSize: 18.0),
      //       onTap: () => themeNotifier.setTheme(greenTheme),
      //       onLongPress: () => print('THIRD CHILD LONG PRESS'),
      //     ),
      //     SpeedDialChild(
      //       child: Icon(Icons.color_lens_outlined),
      //       backgroundColor: Colors.pink,
      //       // label: 'Third',
      //       // labelStyle: TextStyle(fontSize: 18.0),
      //       onTap: () => themeNotifier.setTheme(pinkTheme),
      //       onLongPress: () => print('THIRD CHILD LONG PRESS'),
      //     ),
      //   ],
      // ),
    );
  }
}

class stageredProduct extends StatelessWidget {
  String img, price, title;

  stageredProduct({
    Key key,
    this.img,
    this.price,
    this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
          child: Image.asset(
            "assets/banner.jpg",
            fit: BoxFit.fill,
            height: 150,
            width: SizeConfig.w * 0.4,
          ),
        ),
        Text("product"),
      ],
    );
  }
}
